<!doctype html>
<html lang="de">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Vertretungsplan der Ferdinand-von-Steinbeisschule-Reutlingen</title>
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   <link rel="stylesheet" href="css/style.css">
   <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   <script  src="js/index.js"></script>
</head>
<body>

<?php

  error_reporting(E_ALL);

  $self=$_SERVER['PHP_SELF'];
  isset($_POST['user']) ? $user=$_POST['user'] : $user="";
  isset($_POST['pass']) ? $pass=$_POST['pass'] : $pass="";
  isset($_POST['logout']) ? $logout=$_POST['logout'] : $logout=FALSE;


  include_once("vars.php");
  include_once("functions.php");
  include_once("seiten.php");

  session_start();

  echo "<script>\n";
  echo "function proceed (value) {
  	  var kalenderwoche=$kw+value;
      var form = document.createElement('form');
      form.setAttribute('method', 'post');
      form.setAttribute('action', '$self');

      var hf1 = document.createElement(\"input\");
            hf1.setAttribute(\"type\", \"hidden\");
            hf1.setAttribute(\"name\", \"user\");
            hf1.setAttribute(\"value\", \"$user\");
            form.appendChild(hf1);

      var hf2 = document.createElement(\"input\");
            hf2.setAttribute(\"type\", \"hidden\");
            hf2.setAttribute(\"name\", \"pass\");
            hf2.setAttribute(\"value\", \"$pass\");
            form.appendChild(hf2);

      var hf3 = document.createElement(\"input\");

            hf3.setAttribute(\"type\", \"hidden\");
            hf3.setAttribute(\"name\", \"kw\");
            hf3.setAttribute(\"value\", kalenderwoche);
            form.appendChild(hf3);

            form.style.display = 'hidden';
            $(document.body).append(form);

            form.submit();
   }";
   echo "function logout (value) {
  	  var kalenderwoche=$kw+value;
      var form = document.createElement('form');
      form.setAttribute('method', 'post');
      form.setAttribute('action', '$self');

      var hf1 = document.createElement(\"input\");
            hf1.setAttribute(\"type\", \"hidden\");
            hf1.setAttribute(\"name\", \"logout\");
            hf1.setAttribute(\"value\", \"TRUE\");
            form.appendChild(hf1);

            form.style.display = 'hidden';
            $(document.body).append(form);

            form.submit();
   }";
  echo "</script>\n";

  if($logout) {
    session_destroy();
  	echo $seite_logout;
  	exit(0);
  }

  // Prüfen ob Logindaten eingegeben wurden
  if ($user=="" || !$_POST['pass'] || !$_POST['kw']) {
	echo $seite_login;
	session_destroy();
  }
  else {  // Fehlermeldung bei falschem Benutzernamen und/oder Passwort
    if(($userdn=authenticate($_POST['user'], $_POST['pass']))==false)
	  {
	  	echo $seite_login;
	  	echo $seite_login_error;
	  	session_destroy();
	  }
	  else
	  {
	  	$_SESSION['username']=$_POST['user'];
	  	$kw=$_POST['kw'];
  		$lindex=array();
  		$userdata=getUserData($userdn, $_POST['pass']);
      if ($userdata["isLehrer"])
      {
        $ppath=getcwd()."/plaene/".$kw."/t/";
        $findex=$ppath."index.txt";
    		$lindex=getIndex($findex);
        $file=$lindex[$userdata['kuerzel']];
        $plan = file_get_contents("plaene/".$kw."/t/m".$file);
      }
      else if($userdata["isSchueler"])
      {
        $ppath=getcwd()."/plaene/".$kw."/c/";
        $findex=$ppath."index.txt";
        $lindex=getIndex($findex);
        $kindex=getIndex("klassen.csv");
        $file=$lindex[$kindex[$userdata['klasse']]];
        $plan = file_get_contents("plaene/".$kw."/c/m".$file);
      }

  		echo "<h2>Stunden- Vertretungsplan für $userdata[name], $userdata[vorname]</h2>\n ";
  		//$date=getWeekDays($kw);
      if(array_key_exists("klasse", $userdata)) echo "<h2>Klasse $userdata[klasse]</h2>";
  		echo "<p class=\"center\">Kalenderwoche ".$kw."</p>\n";
  		echo <<< EOT
  		<form id="rendered-form">
    			<div class="rendered-form">
      			<div class="fb-button form-group field-button-minus">
        				<button type="button" class="btn btn-info" onclick="proceed(-1);" name="button-minus"  style="info" id="button-minus">vorherige Woche<br></button>
      			</div>
      			<div class="fb-button form-group field-button-plus">
        				<button type="button" class="btn btn-info" onclick="proceed(0);" name="button" style="info" id="button">aktuelle Woche<br></button>
      			</div>
      			<div class="fb-button form-group field-button-plus">
        				<button type="button" class="btn btn-info" onclick="proceed(+1);" name="button-plus" style="info" id="button-plus">nächste Woche<br></button>
      			</div>

      			<input type="hidden" value="$user" name="user" />
      			<input type="hidden" value="$pass" name="pass" />
    			</div>
  		</form>
EOT;
  		//echo "<main id=\"Inhalte-werden-zentriert\">";
  		echo "<div id=\"zentriert\">";
  		echo "<div class=\"table-scrollable \">";
          echo $plan;
  		echo "</div></div>";
  		//echo "</main>";
  		echo <<< EOT
  		<form id="rendered-form">
    			<div class="rendered-form">
      			<div class="fb-button form-group field-button-minus">
        				<button type="button" class="btn btn-primary" onclick="logout();" name="button-logout"  style="info" id="button-logout">Logout<br></button>
      			</div>
      		</div>
  		</form>
EOT;
	  }
  }

?>
</body>
</html>
