<?php

$kw=date('W');

$seite_login = <<<EOF
<div class="content">
  	<div class="login-page">
		<div class="login-page">
		  <div class="form">
	     	<img src="logo.jpg" class="logo" alt="Logo Steinbeis-Schule">
		    <h1>Vertretungsplan</h1>
		    <form class="login-form" method="post" action="$self">
		      <input name="user" type="text" placeholder="PaedNET-Loginname"/>
		      <input name="pass" type="password" placeholder="Passwort"/>
		      <input type="hidden" value="$kw" name="kw" />
		      <button>login</button>
		    </form>
		  </div>
		</div>
   </div>
</div>
EOF;

$seite_logout = <<<EOF
<div class="content">
  	<div class="login-page">
		<div class="login-page">
		  <div class="form">
	     	<img src="logo.jpg" class="logo" alt="Logo Steinbeis-Schule">
		    <h1>Logout erfolgreich</h1>
		    <form class="login-form" method="post" action="$self">
		      <button>Erneut anmelden</button>
		    </form>
		  </div>
		</div>
   </div>
</div>
EOF;

$seite_login_error = <<<EOF
<!-- Modal -->
  <div class="modal fade" id="authErrorModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Authentifizierungsfehler</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p class="error">Sie haben einen nicht gültigen Benutzernamen und/oder Passwort eingegeben. Bitte versuchen Sie es erneut.</p>
        <!--  <p class="error">Bitte beachten Sie, dass die Login-Seite berücksichtigt Groß-/Kleinschreibung berücksichtigt. Bei Lehreraccounts sollten Sie <font face="monospace">L_XXX</font> oder <font face="monospace">L_XXX-fvs</font> schreiben. Bei Schüleraccounts <font face="monospace">RachmannA-fvs</font>.</p>-->
          <p class="error">Falls das Problem weiterhin bestehen sollte, melden Sie sich unter <a href="mailto:support@steinbeisschule-reutlingen.de"> support@steinbeisschule-reutlingen.de</a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
        </div>
      </div>

    </div>
  </div>

</div>

EOF;
?>
