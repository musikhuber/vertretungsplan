<?php

include_once("vars.php");

function authenticate($user, $password) {
	if (empty($user) || empty($password))	return false;

  $authenitcated=false;

  global $ldap_host;
	global $ldap_bind_user;
	global $ldap_bind_pw;

	global $basedn;
	global $searchstring;
	global $attnames;


	$ds = ldap_connect($ldap_host, 389);

	if ($ds) {
		if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)) {
			print "Could not set LDAPv3\r\n";
		}
		else {
			// Bind mit LDAPuserWWW ausführen
			$r = ldap_bind($ds, $ldap_bind_user, $ldap_bind_pw) or die("\r\nCould not connect to LDAP server 1\r\n");
			if (!$r)
				echo "ldap_bind nobind <br>";
			else {
				$r = ldap_search($ds, $basedn, $searchstring, $attnames);
				$data = ldap_get_entries($ds, $r);
				// iterate over array and print data for each entry

				for ($i = 0; $i < $data["count"]; $i++) {
					if(strtolower($data[$i]["cn"][0])==strtolower($user))  // find user
					{
					   $userdn=$data[$i]["dn"];
					}
				}
			}
		}
		ldap_close($ds);
	}
	else {
		echo "<h4>Unable to connect to LDAP server</h4>";
	}

	$ds = ldap_connect($ldap_host, 389);
	if ($ds) {
		if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)) {
			print "Could not set LDAPv3\r\n";
		}
		else {
			// Bind mit USERDN
			$r = ldap_bind($ds, $userdn, $password);
			if($r) $authenitcated=$userdn;

		}
		ldap_close($ds);
	}
	else {
		echo "<h4>Unable to connect to LDAP server</h4>";
	}
	return $authenitcated;
}


function getUserData($userdn, $password)
{
	if (empty($userdn) || empty($password))	return false;

  global $ldap_host;
	global $ldap_bind_user;
	global $ldap_bind_pw;

	global $basedn;
	$searchstring = "(objectclass=user)";
	$attnames = array("dn", "ou", "cn", "surname", "givenName", "mail");

	$return=array();


	$ds = ldap_connect($ldap_host, 389);

	if ($ds) {
		if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)) {
			print "Could not set LDAPv3\r\n";
		}
		else {
	 		// Bind mit userdn ausführen
			$r = ldap_bind($ds, $userdn, $password) or die("\r\nCould not connect to LDAP server 1\r\n");
			if (!$r)
				echo "ldap_bind nobind <br>";
			else {
				$r = ldap_search($ds, $userdn, $searchstring, $attnames);
				$data = ldap_get_entries($ds, $r);

				$return["isLehrer"]=false;
				$return["isSchueler"]=false;
				if(preg_match('/ou=Lehrer/i', $userdn))
				{
					$return["isLehrer"]=true;
					$return["isSchueler"]=false;

					$zeichenkette=$data[0]["cn"][0];
					$suchmuster='/(L_)(\w+)(-fvs)?/';
					$ersetzung='$2';
					$return["kuerzel"]=preg_replace($suchmuster, $ersetzung, $zeichenkette);
				}
				else if (preg_match('/ou=Schueler/i', $userdn))
				{
					$return["isLehrer"]=false;
					$return["isSchueler"]=true;
					$return["klasse"]=$data[0]["ou"][0];
				}

				$return["name"]=$data[0]["surname"][0];
				$return["vorname"]=$data[0]["givenname"][0];

			}
		}
		ldap_close($ds);
	}
	else {
		echo "<h4>Unable to connect to LDAP server</h4>";
	}

	return $return;

}

function getIndex($indexfile)
{
	if (($handle = fopen($indexfile, "r")) !== FALSE)
	{
			while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE)
			{
					$lindex[$data[0]]=$data[1];
			}
			fclose($handle);
	}
	else echo "Konnte index.txt nicht öffnen\n";

	return $lindex;
}

?>
