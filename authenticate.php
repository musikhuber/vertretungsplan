<?php

include_once("vars.php");

function authenticate($user, $password) {
	if (empty($user) || empty($password))
		return false;

    $authenitcated=false;
	
    global $ldap_host;
	global $ldap_bind_user;
	global $ldap_bind_pw;

	global $basedn;
	global $searchstring;
	global $attnames;
	

	$ds = ldap_connect($ldap_host, 389);

	if ($ds) {
		if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)) {
			print "Could not set LDAPv3\r\n";
		} 
		else {
			// Bind mit LDAPuserWWW ausführen
			$r = ldap_bind($ds, $ldap_bind_user, $ldap_bind_pw) or die("\r\nCould not connect to LDAP server 1\r\n");
			if (!$r)
				echo "ldap_bind nobind <br>";
			else {
				$r = ldap_search($ds, $basedn, $searchstring, $attnames);
				$data = ldap_get_entries($ds, $r);
				// iterate over array and print data for each entry
				
				for ($i = 0; $i < $data["count"]; $i++) {
					if($data[$i]["cn"][0]==$user)  // find user
					{
					   $userdn=$data[$i]["dn"];             	              		  
					}		
				}
			}
		}
		ldap_close($ds);
	}
	else {
		echo "<h4>Unable to connect to LDAP server</h4>";
	}  
		
	$ds = ldap_connect($ldap_host, 389);
	if ($ds) {
		if (!ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3)) {
			print "Could not set LDAPv3\r\n";
		} 
		else {
			// Bind mit USERDN
			$r = ldap_bind($ds, $userdn, $password);
			if($r) $authenitcated=$userdn;
			
		}
		ldap_close($ds);
	}
	else {
		echo "<h4>Unable to connect to LDAP server</h4>";
	}  		
	return $authenitcated;
}
?>